'use strict';
var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    config = require('./config');

// Static Server + watching scss/html files
gulp.task('serve', ['build:css'], function() {

    browserSync.init({
        server: "./src"
    });

    gulp.watch('./src/sass/*.scss', ['build:css']);
    gulp.watch("src/templates/**/*.html").on('change', browserSync.reload);
});

gulp.task('build:css', function () {
    return gulp.src('./src/sass/*.scss')
        .pipe(sass({errLogToConsole: true}))
        //.pipe(autoprefixer(config.get('css')))
        .pipe(gulp.dest('./src/assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function () {
    gulp.watch('./src/sass/*.scss', ['build:css']);
});

gulp.task('default', ['serve']);
