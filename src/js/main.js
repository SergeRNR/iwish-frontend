'use strict';
require.config({
    shim: {
        bootstrap: ['jquery']
    },
	paths: {
        backbone: '../libs/backbone',
        bootstrap: '../libs/bootstrap',
		jquery: '../libs/jquery',
        marionette: '../libs/backbone.marionette',
		underscore: '../libs/underscore'
	}
});

require(['jquery', 'app', 'models/user', 'models/company'],
function ($, App, User, Company) {
    var locale = localStorage.getItem('locale') || 'en';

    window.aw = {
        App: App,
        locale: locale,
        t: function (str) {
            return this.i18n[str] || str;
        }
    };


    App.user = new User();
    App.company = new Company();

    $.when(
        App.user.fetch(),
        App.company.fetch(),
        $.getJSON(['js/i18n/', locale, '.json'].join(''))
    ).done(function (userResp, companyResp, jsonResp) {
        window.aw.i18n = jsonResp[0];
        App.start();
    });
});