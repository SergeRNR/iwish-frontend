'use strict';
define([
    'backbone',
    'models/event'
], function (Backbone, EventModel) {
    var EventsCollection = Backbone.Collection.extend({

        initialize: function (options) {
            this.fetch();
        },

        model: EventModel,

        url: 'js/collections/events.json'
    });

    return EventsCollection;
});
