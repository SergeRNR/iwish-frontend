'use strict';
define([
    'backbone',
    'models/wish'
], function (Backbone, ItemModel) {
    var ItemsCollection = Backbone.Collection.extend({

        initialize: function (options) {
            this.fetch();
        },

        model: ItemModel,

        url: 'js/collections/wishes.json'
    });

    return ItemsCollection;
});
