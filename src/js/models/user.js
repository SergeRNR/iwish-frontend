'use strict';
define([
    'backbone'
], function (Backbone) {
    var User = Backbone.Model.extend({
        defaults: {
            img: 'assets/img/avatar.png'
        },

        url: 'js/models/user.json'
    });

    return User;
});
