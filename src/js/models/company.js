'use strict';
define([
    'backbone'
], function (Backbone) {
    var Company = Backbone.Model.extend({

        url: 'js/models/company.json'
    });

    return Company;
});
