'use strict';
define([
    'backbone'
], function (Backbone) {
    var Event = Backbone.Model.extend({
        defaults: {
        }
    });

    return Event;
});
