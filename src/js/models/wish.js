'use strict';
define([
    'backbone'
], function (Backbone) {
    var Item = Backbone.Model.extend({
        defaults: {
            img: 'assets/img/gift.png',
            description: 'Gift description'
        }
    });

    return Item;
});
