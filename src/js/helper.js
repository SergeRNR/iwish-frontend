define([
    'jquery',
    'underscore'
], function ($, _) {

    var config = {
        urlPrefix: '',
        templatesFolder: '../templates/',
        templatesExt: '.html'
    };

    var getTemplate = function (tmplName) {
        var ajax = $.ajax({
                url: [
                    config.urlPrefix,
                    config.templatesFolder,
                    tmplName,
                    config.templatesExt
                ].join(''),
                method: 'GET',
                async: false
            });
        return ajax.responseText;
    };

    var compile = function (tmplName, model) {
        if (model) {
            return _.template(getTemplate(tmplName))(model);
        } else {
            return _.template(getTemplate(tmplName));
        }

    };

    return {
        compile: compile,
        getTemplate: getTemplate
    };
});