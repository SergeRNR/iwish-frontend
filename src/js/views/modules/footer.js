define([
    'marionette',
    'helper',
    'underscore'
], function (Mn, helper, _) {
    var footerData = {
        langs: [
            {title: 'English', value: 'english'},
            {title: 'Russian', value: 'russian'},
            {title: 'German', value: 'german'}
        ],
        countries: [
            {title: 'Belarus', value: 'Belarus'},
            {title: 'Germany', value: 'Germany'},
            {title: 'Finland', value: 'Finland'},
            {title: 'France', value: 'France'},
            {title: 'Italy', value: 'Italy'},
            {title: 'Lithuania', value: 'Lithuania'},
            {title: 'UK', value: 'UK'},
            {title: 'USA', value: 'USA'}
        ],
        discover: [
            {title: 'About aiWeesh', link: '#'},
            {title: 'aiWeesh Blog', link: '#'},
            {title: 'Media about us', link: '#'},
            {title: 'Help', link: '#'},
            {title: 'Terms', link: '#'},
            {title: 'Privacy', link: '#'}
        ],
        join: [
            {title: 'Sign Up', link: '#'},
            {title: 'Invite Friends', link: '#'},
            {title: 'Public Wishes', link: '#'},
            {title: 'Site map', link: '#'}
        ],
        business: [
            {title: 'Business Sign Up', link: '#'},
            {title: 'View all Business Partners', link: '#'}
        ]
    };

    var FooterView = Marionette.LayoutView.extend({
        template: helper.compile('modules/footer', footerData)
    });
    return FooterView;
});