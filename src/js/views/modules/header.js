define([
    'marionette',
    'helper'
], function (Mn, helper) {
    var HeaderView = Marionette.LayoutView.extend({

        initialize: function (options) {
            if (options.isBusiness) {
                this.template = helper.compile('modules/header-bs');
            } else {
                this.template = helper.compile('modules/header');
            }
        }
    });
    return HeaderView;
});