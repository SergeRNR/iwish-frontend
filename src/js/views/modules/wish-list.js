'use strict';
define([
    'marionette',
    'helper',
    'views/blocks/wish'
], function (Mn, helper, WishView) {
    var WishListView = Mn.CollectionView.extend({

        className: 'aw-wish-list',

        childView: WishView
    });

    return WishListView;
});