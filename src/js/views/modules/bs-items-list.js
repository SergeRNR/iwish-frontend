'use strict';
define([
    'marionette',
    'helper',
    'views/blocks/wish'
], function (Mn, helper, ItemView) {
    var ItemsListView = Mn.CompositeView.extend({

        template: helper.compile('modules/bs-items-list'),

        className: 'aw-items-list',

        childView: ItemView,

        childViewContainer: '.aw-bs-items'
    });

    return ItemsListView;
});