define([
    'marionette',
    'helper'
], function (Mn, helper) {
    var NavigationView = Marionette.LayoutView.extend({

        initialize: function (options) {
            if (options.isBusiness) {
                this.template = helper.compile('modules/navigation-bs');
            } else {
                this.template = helper.compile('modules/navigation');
            }
        }
    });
    return NavigationView;
});