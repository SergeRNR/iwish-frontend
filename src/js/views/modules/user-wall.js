'use strict';
define([
    'marionette',
    'helper',
    'views/blocks/wall-item'
], function (Mn, helper, WallItemView) {
    var WallView = Mn.CompositeView.extend({

        template: helper.compile('modules/user-wall'),

        className: 'user-wall module-content',

        childView: WallItemView,

        childViewContainer: '.wall-messages-list'
    });

    return WallView;
});