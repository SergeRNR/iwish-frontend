'use strict';
define([
    'marionette',
    'helper',
    'views/blocks/event'
], function (Mn, helper, EventView) {
    var EventsListView = Mn.CollectionView.extend({

        className: 'aw-events-list',

        childView: EventView
    });

    return EventsListView;
});