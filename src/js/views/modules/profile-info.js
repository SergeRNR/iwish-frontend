'use strict';
define([
    'marionette',
    'helper'
], function (Mn, helper) {
    var InfoView = Mn.LayoutView.extend({

        render: function () {
            var content;
            if (this.options.isBusiness) {
                content = helper.compile('modules/profile-info-bs', aw.App.company.toJSON());
            } else {
                content = helper.compile('modules/profile-info-user', aw.App.user.toJSON());
            }
            this.$el.html(content);
            return this;
        },

        className: 'profile-info'
    });

    return InfoView;
});