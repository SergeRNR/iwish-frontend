'use strict';
define([
    'marionette',
    'helper',
    'views/modules/header',
    'views/modules/navigation',
    'views/modules/footer'
], function (Mn, helper, HeaderView, NavigationView, FooterView) {
    var RootView = Marionette.LayoutView.extend({

        el: '#main',

        template: helper.compile('root'),

        regions: {
            header: '#aw-header',
            navigation: '#aw-navigation',
            content: '#aw-content',
            footer: '#aw-footer'
        },

        initialize: function () {
            this.render();
            this.showChildView('header', new HeaderView());
            this.showChildView('navigation', new NavigationView());
            this.showChildView('footer', new FooterView());
        }
    });

    return RootView;
});