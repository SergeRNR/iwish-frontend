'use strict';
define([
    'marionette',
    'helper'
], function (Mn, helper) {
    var EventView = Mn.ItemView.extend({

        template: helper.compile('blocks/event'),

        className: 'event-item'
    });

    return EventView;
});