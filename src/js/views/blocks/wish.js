'use strict';
define([
    'marionette',
    'helper'
], function (Mn, helper) {
    var WishView = Mn.ItemView.extend({

        template: helper.compile('blocks/wish'),

        className: 'wish-thumbnail'
    });

    return WishView;
});