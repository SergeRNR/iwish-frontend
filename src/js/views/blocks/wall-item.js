'use strict';
define([
    'marionette',
    'helper'
], function (Mn, helper) {
    var WallItemView = Mn.ItemView.extend({

        template: helper.compile('blocks/wall-item'),

        className: 'wall-item'
    });

    return WallItemView;
});