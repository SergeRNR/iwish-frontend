define([
    'marionette',
    'helper'
], function (Mn, helper) {
    var BaseModal = Mn.ItemView.extend({

        el: '#modalContainer',

        initialize: function(options){
            this.template = helper.compile('modal/base-modal', options);
        },

        show: function () {
            this.render();
            this.$el.find('#modal').modal('show');
        },

        close: function () {
            this.$el.find('#modal').modal('hide');
        }

    });
    return BaseModal;
});