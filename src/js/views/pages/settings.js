define([
    'marionette',
    'helper',
    'underscore'
], function (Mn, helper, _) {
    var SettingsView = Marionette.LayoutView.extend({
        template: helper.compile('pages/settings')
    });
    return SettingsView;
});