'use strict';
define([
    'marionette',
    'helper',
    'views/modules/profile-info'
], function (Mn, helper, InfoView) {
    var IwishlistView = Marionette.LayoutView.extend({

        template: helper.compile('pages/iwishlist'),

        regions: {
            'user-info': '#aw-user-info'
        },

        onRender: function () {
            this.showChildView('user-info', new InfoView());
        }
    });
    return IwishlistView;
});