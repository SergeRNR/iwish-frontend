'use strict';
define([
    'marionette',
    'backbone',
    'helper',
    'collections/wishes',
    'collections/events',
    'views/modules/profile-info',
    'views/modules/events-list',
    'views/modules/wish-list',
    'views/modules/user-wall'
], function (Mn, Backbone, helper, WishesCollection, EventsCollection,
             InfoView, EventsView, WishListView, WallView) {

    var ProfilePage = Marionette.LayoutView.extend({

        name: 'eventsPage',

        template: helper.compile('pages/profile'),

        regions: {
            'user-info': '#aw-user-info',
            'coming-events': '#aw-coming-events',
            'random-wishes': '#aw-random-wishes',
            'wish-ideas': '#aw-wish-ideas',
            'wall': '#aw-user-wall'
        },

        onRender: function () {
            var wallView = new WallView({
                collection: new Backbone.Collection([
                    {message: 'Hi User Name! Happy Birthday and have a lot of fun riding your new bicycle!'},
                    {message: 'Hi User Name! Happy Birthday and have a lot of fun riding your new bicycle!'}
                ])
            });

            this.showChildView('user-info', new InfoView({isUser: true}));

            this.showChildView('coming-events', new EventsView({
                collection: new EventsCollection()
            }));

            this.showChildView('random-wishes', new WishListView({
                collection: new WishesCollection()
            }));

            this.showChildView('wish-ideas', new WishListView({
                collection: new WishesCollection()
            }));

            this.showChildView('wall', wallView);
        }
    });

    return ProfilePage;
});