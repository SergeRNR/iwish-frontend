'use strict';
define([
    'marionette',
    'backbone',
    'helper',
    'views/modules/profile-info',
    'views/modules/bs-items-list',
    'collections/wishes'
], function (Mn, Backbone, helper, InfoView, ItemsView, ItemsCollection) {
    var ProfileView = Marionette.LayoutView.extend({

        template: helper.compile('pages/bs-profile'),

        regions: {
            'bs-info': '#aw-bs-info',
            'bs-items': '.aw-bs-items'
        },

        onRender: function () {
            this.showChildView('bs-info', new InfoView({isBusiness: true}));
            this.showChildView('bs-items', new ItemsView({
                collection: new ItemsCollection()
            }));
        }
    });

    return ProfileView;
});