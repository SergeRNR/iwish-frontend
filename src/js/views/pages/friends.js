'use strict';
define([
    'marionette',
    'helper',
    'views/modules/profile-info'
], function (Mn, helper, InfoView) {
    var FriendsView = Marionette.LayoutView.extend({

        template: helper.compile('pages/friends'),

        regions: {
            userinfo: '#aw-userinfo'
        },

        onRender: function () {
            this.showChildView('userinfo', new InfoView());
        }
    });
    return FriendsView;
});