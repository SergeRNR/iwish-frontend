define([
    'marionette',
    'helper',
    'underscore'
], function (Mn, helper, _) {
    var MessagesView = Marionette.LayoutView.extend({
        template: helper.compile('pages/messages')
    });
    return MessagesView;
});