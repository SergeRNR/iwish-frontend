'use strict';
define([
    'marionette',
    'helper',
    'collections/events',
    'views/modules/profile-info',
    'views/modules/events-list'
], function (Mn, helper, EventsCollection, InfoView, EventsView) {
    var EventsPage = Marionette.LayoutView.extend({

        template: helper.compile('pages/events'),

        regions: {
            'user-info': '#aw-user-info',
            'all': '#aw-events-all .events-container',
            'my': '#aw-events-my .events-container',
            'friends': '#aw-events-friends .events-container',
            'holidays': '#aw-events-holidays .events-container'
        },

        onRender: function () {
            this.showChildView('user-info', new InfoView({isUser: true}));

            this.showChildView('all', new EventsView({
                collection: new EventsCollection()
            }));

            this.showChildView('my', new EventsView({
                collection: new EventsCollection()
            }));

            this.showChildView('friends', new EventsView({
                collection: new EventsCollection()
            }));

            this.showChildView('holidays', new EventsView({
                collection: new EventsCollection()
            }));

            if (this.options.tab) {
                this.showTab(this.options.tab);
            }
        },

        showTab: function (tab) {
            this.$el.find('a[href=#aw-events-' + tab + ']').tab('show');
        }
    });

    return EventsPage;
});