define([
    'marionette',
    'helper',
    'underscore',
    'views/modules/header',
    'views/modules/navigation',
    'views/pages/profile',
    'views/pages/iwishlist',
    'views/pages/events',
    'views/pages/friends',
    'views/pages/messages',
    'views/pages/settings',

    'views/pages/bs-profile'
], function (Mn, helper, _, HeaderView, NavView, ProfileView, IwishlistView,
             EventsView, FriendsView, MessagesView, SettingsView, BSProfileView) {

    var Router = Marionette.AppRouter.extend({
        routes : {
            '': 'defaultRoute',
            'profile' : 'profile',
            'iwishlist': 'iwishlist',
            'iwishlist/:tab': 'iwishlist',
            'events': 'events',
            'events/:tab': 'events',
            'friends': 'friends',
            'friends/:tab': 'friends',
            'messages': 'messages',
            'messages/:tab': 'messages',
            'settings': 'settings',

            'business': 'bs-default',
            'business/profile': 'bs-profile'
        },

        initialize: function (options) {
            // TODO: remove link to App and import as module
            this.App = options.app;
            this.isBusiness = false;
        },

        renderContentView: function (view) {
            if (this.isBusiness) {
                this.isBusiness = false;
                this.App.rootView.showChildView('header', new HeaderView({isBusiness: false}));
                this.App.rootView.showChildView('navigation', new NavView({isBusiness: false}));
            }
            this.activeView = view;
            this.App.rootView.showChildView('content', view);
        },

        renderBusinessView: function (view) {
            if (!this.isBusiness) {
                this.isBusiness = true;
                this.App.rootView.showChildView('header', new HeaderView({isBusiness: true}));
                this.App.rootView.showChildView('navigation', new NavView({isBusiness: true}));
            }
            this.App.rootView.showChildView('content', view);
        },

        defaultRoute: function () {
            this.navigate('profile');
        },

        profile : function () {
            this.renderContentView(new ProfileView());
        },

        iwishlist : function (tab) {
            this.renderContentView(new IwishlistView());
        },

        events : function (tab) {
            if (this.activeView instanceof EventsView) {
                this.activeView.showTab(tab);
            } else {
                this.renderContentView(new EventsView({tab: tab}));
            }
        },

        friends : function (tab) {
            this.renderContentView(new FriendsView());
        },

        messages : function () {
            this.renderContentView(new MessagesView());
        },

        settings: function () {
            this.renderContentView(new SettingsView());
        },

        'bs-default': function () {
            this.navigate('business/profile');
        },

        'bs-profile': function () {
            this.renderBusinessView(new BSProfileView());
        }
    });

    return Router;
});