define([
	'jquery',
	'underscore',
    'backbone',
    'marionette',
    'router',
    'views/root',
    'bootstrap'
], function ($, _, Backbone, Mn, Router, RootView) {
    var App = new Mn.Application();

    App.on('start', function () {
        App.rootView = new RootView();
        new Router({app: App});
        Backbone.history.start();
    });

    return App;
});
